﻿# -*- coding: utf-8 -*-
import random
from Functions import *
import threading
import time
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor

class RSA(object):

    minimumSizeKey = 7;

    prime_numbers = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257]
    
    def __init__(self, p = 0, q = 0, e = 0, d = 0, sizeKey = minimumSizeKey):
        self.p = p;
        self.q = q;

        if sizeKey < RSA.minimumSizeKey:
            raise Exception("Минимум " + str(RSA.minimumSizeKey) + " бит");
        
        n = self.p * self.q;
        if  n < 19999 and n != 0:
            raise Exception("Маленькие параметры p и q. Должно быть p * q > 1999")
        
        if self.p != 0 and self.q != 0:
            if self.__p_and_q_is_prime__():
                self.__initKeys__(e, d)
        else:
            start = time.clock()
            while self.p * self.q < 19999:
                if self.p < 140:
                    self.p = self.generateNumerP(sizeKey)
                if self.q < 140:
                    self.q = self.getPrimeNumber(sizeKey);
            stop = time.clock()
            self.__initKeys__(e, d)
            print("Generate prime number time = ",stop - start)
        
    def __initKeys__(self, e, d):
        p = self.p
        q = self.q
        self.n = p * q;
        self.fi_n = (p-1) * (q-1)  # функция Эйлера
        if e == 0 and d == 0:
            self.generateKeys();
        else:
            self.e = e;
            self.d = d;

    def __p_and_q_is_prime__(self):
        if not RSA.testMillerRabin(self.p, int(math.log(self.p, 2))):
            raise Exception("p должно быть простым")
        else:
            if not RSA.testMillerRabin(self.q , int(math.log(self.q , 2))):
                raise Exception("q должно быть простым")
        return True;

    def getAllParametrs(self):
        dictionaryKeys = {
                            "Открытый ключ": [self.e, self.n],
                            "Закрытый ключ": [self.d, self.n],
                            "Функция Эйлера": self.fi_n,
                            "p*q": self.n
                          }
        return dictionaryKeys;

    def generateKeys(self):
        start = time.time()
        self.e = self.chooseOpenExponent();
        stop = time.time()
        print("Exp time = ", stop - start)
        self.d = self.euclidExtended(self.e, self.fi_n);

    def generateParametrs(self, sizeKey):
        start = time.clock()
        result_p = self.generateNumerP(sizeKey)
        #result_p = self.getPrimeNumber(sizeKey);
        result_q = self.getPrimeNumber(sizeKey);
        stop = time.clock()
        print("Generate prime number time = ",stop - start)

        return result_p, result_q;

    def divPrimeNumbers(self, n):
        for number in RSA.prime_numbers:
            if n > number:
                return True
            if n % number == 0:
                return False;
        return True;

    def generateNumerP(self, size):
        s = self.getPrimeNumber(size//2)
        #print("S = " + str(s));
        while True:
            r = random.randint(3, 4*s + 2)
            n = s*r + 1

            if self.divPrimeNumbers(n):
                if RSA.testMillerRabin(n, int(math.log(n, 2))):
                    a = random.randint(1, n)
                    while True: 
                        #print("Выбираю A")
                        a = random.randint(1, n)
                        if pow(a, n - 1, n) == 1 or self.gcd(pow(a, r) - 1, n ) == 1:
                            return n;

    def getPrimeNumber(self, size):
        n = 1;
        #не подходят числа 2, 3 из-за Теста Ферма, МР
        while n < 4:
            n = random.getrandbits(size);
        while True:
            #a = random.getrandbits(size);
            #if self.gcd(n, a) == True and pow(a, n - 1, n) == 1:
            if self.testFerma(n):
                if RSA.testMillerRabin(n, int(math.log(n,2))):
                    return n;
            n += 1;
    
    #ord('b') = 97 = '0097'
    def getCodeInStr(symbol):
        ch = ord(symbol);
        if ch > 1000:
            return str(ch);
        countDigit = 0;
        ch_copy = ch
        while ch_copy != 0:
            ch_copy //= 10;
            countDigit += 1;
        return '0'*(4 - countDigit) + str(ch);

    def encodingMessage(message, e, n):
        lengthMessage = len(message);
        if lengthMessage != 0:
            result = "";
            symbolIndex = 0;
            codeMessage = "1";
            while symbolIndex < len(message):
                code = RSA.getCodeInStr(message[symbolIndex]);
                if int(codeMessage + code) < n - 1:
                    codeMessage += code;
                    symbolIndex += 1;
                else:
                    result += str(pow(int(codeMessage), e, n)) + " ";
                    codeMessage = "1"
            else:
                result += str(pow(int(codeMessage), e, n)) + " ";
            return result;
        else:
            raise Exception("Пустое сообщение");
            # Exception("Пустая строка");

    def decodingMessage(message, d, n):
        lengthMessage = len(message);
        if lengthMessage != 0:
            result = "";
            listMessage = message.split(" ");
            listMessage.pop(len(listMessage) - 1);
            for partMessage in listMessage:
                partMessage_int = int(partMessage)
                codeMessage = str(int(pow(partMessage_int, d,  n)))
                codeMessage = codeMessage[1:len(codeMessage)]
                for index in range(0, len(codeMessage), 4):
                    result += chr(int(codeMessage[index:index+4]));

            return result;
        else:
            raise Exception("Пустое сообщение");

    def testFerma(self, n, iteration = 100):
        if n == 2:
            return True;
        for _ in range(0, iteration):
            a = random.randint(1, n - 1);
            if self.gcd(a, n) != 1:
                return False;
            if pow(a, n - 1, n) != 1:
                return False;
        return True;

    def ferma(self, a, n):
        return pow(a, n - 1, n) == 1;

    # n - проверяемое число
    # k - количество раундов
    def testMillerRabin(n, k):
        if n < 2:
            return False;
        if n == 2 or n == 3:
            return True;
        if n & 1 == 0:  # % 2
            return False;
        r = 0;
        d = n - 1;

        while d & 1 == 0:
            d = d >> 1
            r += 1;

        for _ in range(k):
            a = random.randint(2, n - 2);
            x = pow(a,d,n)

            if x == 1 or x == n - 1:
                continue;

            for _ in range(r - 1):

                x = pow(x, 2, n);
                if x == 1:
                    return False;
                if x == n - 1:
                    break;
            if x != n - 1:
                return False;
        
        return True;

    def gcd(self, a, b):
        while b != 0:
            temp = a % b;
            a = b;
            b = temp;
        return a;
        
    # выбор открытой экспоненты
    def chooseOpenExponent(self):
        exp = random.randint(self.fi_n // 2 , self.fi_n);
        if exp % 2 == 0:
            exp += 1;
        while True:
            if self.gcd(exp, self.fi_n) == 1:
                return exp;
            else:
                exp += 2;

    #return d
    def euclidExtended(self, a, b):
        copy = max(a, b);
        x,y, u,v = 0,1, 1,0
        while a != 0:
            r = b % a;#get remainder
            q = b//a#get quotient
            m, n = x-u*q, y-v*q#substitution
            b,a, x,y, u,v = a,r, u,v, m,n
        return x + copy;

    def getOpenExp(self):
        return self.e;

    def getSecretExp(self):
        return self.d;

    def getN(self):
        return self.n;

    def getSizeKey(self):
        return self.sizeKey;

    def getEulerFunction(self):
        return self.fi_n;
