﻿# -*- coding: utf-8 -*-
import sys
import random;
import math;

def FermaTest(a, n):
    return pow(a, n - 1, n) == 1;

# n - проверяемое число
# k - количество раундов
def MillerRabinTest(n, k):
    if n < 2:
        return False;
    if n == 2:
        return True;
    if n % 2 == 0:
        return False;
    r = 0;
    d = n - 1;

    while d % 2 == 0:
        d = d // 2;
        r+=1;

    i = 0;
    while i < k:
        i += 1;
        a =random.randint(1, n - 2);
        x = pow(a, d, n)

        if x == 1 or x == n - 1:
            continue;

        j = 0;
        while j < r - 1:
            x = pow(x, 2, n);
            if x == 1:
                return False;
            if x == n - 1:
                break;
            j+=1;
        if x != n - 1:
            return False;
        
    return True;

# функция Эйлера
def EulerFunction(p, q):
    return (p - 1)*(q - 1);

def gcd(a, b):
    while b != 0:
        temp = a % b;
        a = b;
        b = temp;
    return a;

def EuclidExtended(a, b):
    copy = max(a, b);
    x,y, u,v = 0,1, 1,0
    while a != 0:
        r = b % a;#get remainder
        q = b//a#get quotient
        m, n = x-u*q, y-v*q#substitution
        b,a, x,y, u,v = a,r, u,v, m,n
    return x + copy;

# вычислить секретную экспоненту; число обратное к e по модулю;
# с помощью расширенного алгоритма Евклида
def CalculateSecretExponent(e, fi_n):
    return EuclidExtended(e, fi_n);


# генерация ключей
def GenerateKeys(p, q):
    n = p * q;
    fi_n = EulerFunction(p,q);
    openExp = ChooseOpenExponent(fi_n);
    secretExp = CalculateSecretExponent(openExp, fi_n);
    dictionaryKeys = {"Открытый ключ" : [openExp, n] , "Закрытый ключ" : [secretExp, n], "Функция Эйлера" : fi_n, "p*q":n}
    return dictionaryKeys;

def quickPow(a, degree, mod):
    try:
        bi = bin(degree);
        b = bi[2:len(bi) + 1]
        i = 1;
        print();
        oldA = a;
        s = "";
        while i < len(b):
            if b[i] == '0':
                a = (a * a) % mod;
                s = str(a);
            elif b[i] == '1':
                a = oldA * (a*a) % mod;
                s = str(a);
            i+=1;
        return s;
    except:
        raise Exception("Внимание!", "Что-то пошло не так");
