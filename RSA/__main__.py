# -*- coding: utf-8 -*-

import sys
sys.path.append("Core/")
sys.path.append("GUI/")
sys.path.append("GUI/forms")
sys.path.append("GUI/view")
sys.path.append("resource/")

from PyQt5.QtWidgets import QApplication, QMainWindow
from MainForm import MainForm
from Functions import *;
from random import randint
from Ui_MainForm import Ui_MainForm;


if __name__  == '__main__':
    app = QApplication(sys.argv)
    window = QMainWindow()
    mf = MainForm()
    mf.setupUi(window)
    window.show()
    sys.exit(app.exec_())