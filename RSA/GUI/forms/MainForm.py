# -*- coding: utf-8 -*-

import sys
from PyQt5.QtCore import pyqtSlot, QObject
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import (QMainWindow, QPushButton, 
                             QApplication, QMessageBox, 
                             QWidget, QDialog, QAction)
from Ui_MainForm import Ui_MainForm;
from RSA import RSA;
from GUI_APP import *;
from Functions import GenerateKeys, MillerRabinTest;
import math
import functools
import time
import threading


class MainForm(Ui_MainForm, QMainWindow):

    def setupUi(self, MainWindow):
        super(MainForm, self).setupUi(MainWindow);
        self.initUi();

    def initUi(self):

        self.encode_button.clicked.connect(self.on_encode_button_clicked);
        self.decode_button.clicked.connect(self.on_decode_button_clicked); 
        self.generateParametrRSA_button.clicked.connect(self.on_generateParametrRSA_button_clicked);      
        self.clearParametrsRSA_button.clicked.connect(self.on_clearParametrsRSA_button_clicked);
        self.goQuickPow_action.triggered.connect(self.on_goQuickPow_menu_clicked);
        self.goEuclid_action.triggered.connect(self.on_goEuclid_menu_clicked);
        self.clearInputFiled_button.clicked.connect(self.on_clearInputFiled_button_clicked);
        self.generateKeys_button.clicked.connect(self.on_generateKeys_button_clicked);
        
        self.p_edit.textChanged.connect(functools.partial(self.on_edit_textChanged, 
                                                          self.p_isPrime_label,
                                                          self.p_edit))
        self.q_edit.textChanged.connect(functools.partial(self.on_edit_textChanged, 
                                                          self.q_isPrime_label,
                                                          self.q_edit))
        #self.p_edit.editingFinished.connect(functools.partial(self.on_edit_textChanged, 
        #                                                  self.p_isPrime_label,
        #                                                  self.p_edit))
        self.rsa = None;
        self.button_was_pressed_generateKeys = False;

    def on_edit_textChanged(self, component_label, component_edit):      
        try:
            start = time.clock()
            number = int(component_edit.text())
            if self.button_was_pressed_generateKeys == True:
                ico = QtGui.QPixmap(":/ok")
                component_label.setPixmap(ico);
            else:
                if RSA.testMillerRabin(number, int(math.log(number))):
                    ico = QtGui.QPixmap(":/ok")
                    component_label.setPixmap(ico);
                else:
                    ico = QtGui.QPixmap(":/bad")
                    component_label.setPixmap(ico);
                stop = time.clock()
                print("Icon = " + str(stop - start))
        except:
            ico = QtGui.QPixmap(":/update")
            component_label.setPixmap(ico);
        #self.q_edit.setText(str(MillerRabinTest(int(self.p_edit.text()),math.log(int(self.p_edit.text())))))

    def on_generateKeys_button_clicked(self):
        try:
            p = int(self.p_edit.text())
            q = int(self.q_edit.text())
            if p == q:
                raise Exception("<b>p</b> и <b>q</b> должны быть разными");
            if (self.rsa == None) or (self.rsa.p != p or self.rsa.q != q):
                self.rsa = RSA(p, q)
            else:
                self.rsa.generateKeys();

            fi = self.rsa.getEulerFunction()
            self.fi_edit.setText(str(fi));

            e = self.rsa.getOpenExp();
            self.e_edit.setText(str(e));

            d = self.rsa.getSecretExp();
            self.d_edit.setText(str(d));

            n = self.rsa.getN();

            self.n_edit.setText(str(n));
        except ValueError as val:
            len_p = len(self.p_edit.text());
            lep_q = len(self.q_edit.text());
            result_message_error = "Пустые параметры <b>p</b> и <b>q</b>";
            if len_p == 0 and lep_q != 0:
                result_message_error = "Пустой параметр <b>p</b>";
            elif len_p != 0 and lep_q == 0:
                result_message_error = "Пустой параметр <b>q</b>";
            msg = QMessageBox.warning(QWidget().setGeometry(self.centralwidget.geometry()), "Внимание!", result_message_error, QMessageBox.Ok);
        except Exception as exp:
            msg = QMessageBox.warning(QWidget().setGeometry(self.centralwidget.geometry()), "Внимание!", str(exp), QMessageBox.Ok);

    #region Work with Input/Output message

    def on_encode_button_clicked(self):
        try:
            #message
            m = self.originalMessage_edit.toPlainText();
            result = RSA.encodingMessage(m, int(self.e_edit.text()), int(self.n_edit.text()))
            self.codedMessage_edit.setText(str(result));
        except ValueError as val:
            len_p = len(self.p_edit.text());
            lep_q = len(self.q_edit.text());
            result_message_error = "Пустые параметры <b>p</b> и <b>q</b>";
            if len_p == 0 and lep_q != 0:
                result_message_error = "Пустой параметр <b>p</b>";
            elif len_p != 0 and lep_q == 0:
                result_message_error = "Пустой параметр <b>q</b>";
            msg = QMessageBox.warning(QWidget().setGeometry(self.centralwidget.geometry()), "Внимание!", result_message_error, QMessageBox.Ok);
        except Exception as exp:
            msg = QMessageBox.warning(QWidget().setGeometry(self.centralwidget.geometry()), "Внимание!", str(exp), QMessageBox.Ok);

    def on_decode_button_clicked(self):
        try:
            #self.rsa = RSA();
            d = int(self.d_edit.text());
            n = int(self.n_edit.text());
            text = self.codedMessage_edit.toPlainText();     
            result = RSA.decodingMessage(text, d, n);
            self.decodedMessage_edit.setText(str(result));
        except ValueError as val:
            print(str(val));
            len_d = len(self.d_edit.text());
            lep_n = len(self.n_edit.text());
            result_message_error = "Пустые параметры <b>d</b> и <b>n</b>";
            if len_d == 0 and lep_n != 0:
                result_message_error = "Пустой параметр <b>d</b>";
            elif len_d != 0 and lep_n == 0:
                result_message_error = "Пустой параметр <b>n</b>";
            msg = QMessageBox.warning(QWidget().setGeometry(self.centralwidget.geometry()), "Внимание!", result_message_error, QMessageBox.Ok);
        except Exception as exp:
            msg = QMessageBox.warning(QWidget().setGeometry(self.centralwidget.geometry()), "Внимание!", str(exp), QMessageBox.Ok);
    
    def on_clearInputFiled_button_clicked(self):
        self.originalMessage_edit.setText("");
        self.codedMessage_edit.setText("");
        self.decodedMessage_edit.setText("");

    #endregion

    #region Work with parameters RSA

    def generateParametrRSA_helper_thread(self, sizeKey):
        self.parametrsRSA_groupBox.setDisabled(True)
        self.rsa = RSA(sizeKey = sizeKey)
        self.p_edit.setText(str(self.rsa.p));
        self.q_edit.setText(str(self.rsa.q));
        self.fi_edit.setText(str(self.rsa.fi_n));
        self.e_edit.setText(str(self.rsa.e));
        self.d_edit.setText(str(self.rsa.d));
        self.n_edit.setText(str(self.rsa.n));
        self.parametrsRSA_groupBox.setDisabled(False)
        self.button_was_pressed_generateKeys = False;

    def on_generateParametrRSA_button_clicked(self):
        sizeKey = self.sizeKey_edit.text();    
        try:
            self.button_was_pressed_generateKeys = True;
            event = threading.Event()
            thread = threading.Thread(target=self.generateParametrRSA_helper_thread, kwargs = {"sizeKey" : int(sizeKey)})
            thread.start()
            event.set()
        except ValueError as val:
            len_sizeKey = len(sizeKey);
            result_message_error = "Пустой параметр <b>Размер ключа</b>"
            if len_sizeKey != 0:
                if not sizeKey.isdigit():
                    result_message_error = "Размер ключа должен быть в виде числа"
            msg = QMessageBox.warning(QWidget().setGeometry(self.centralwidget.geometry()), "Внимание!", result_message_error, QMessageBox.Ok);
        except Exception as exp:
            msg = QMessageBox.warning(QWidget().setGeometry(self.centralwidget.geometry()), "Внимание!", str(exp), QMessageBox.Ok);

    def on_clearParametrsRSA_button_clicked(self):
        self.p_edit.setText("");
        self.q_edit.setText("");
        self.fi_edit.setText("");
        self.d_edit.setText("");
        self.e_edit.setText("");
        self.n_edit.setText("");
    
    #endregion

    #region Дополнительные инструменты

    def on_goQuickPow_menu_clicked(self):
        self.w = QDialog()
        self.ui = QuickPowForm()
        self.ui.setupUi(self.w)
        self.w.show()

    def on_goEuclid_menu_clicked(self):
        self.w = QDialog()
        self.ui = EuclidForm()
        self.ui.setupUi(self.w)
        self.w.show()

    #endregion

