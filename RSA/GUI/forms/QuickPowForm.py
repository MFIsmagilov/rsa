# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QDialog, QMessageBox, QWidget, QApplication
from Ui_QuickPowForm import Ui_QuickPowForm;
from Functions import quickPow


class QuickPowForm(Ui_QuickPowForm):

    def setupUi(self, MainWindow):
        super(QuickPowForm, self).setupUi(MainWindow);
        self.initUi();

    def initUi(self):
        self.resultQuickPow_button.clicked.connect(self.on_resultQuickPow_button_clicked);
        self.example_label.mousePressEvent = self.on_example_label_mouseDoubleClickEvent

    def on_example_label_mouseDoubleClickEvent(self, event):
        example = self.example_label.text()
        self.expression_edit.setText(example[8:len(example)]);

    def on_resultQuickPow_button_clicked(self):
        try:
            expression = self.expression_edit.text();
            indexPowSymbol = expression.find("^");
            indexModSymbol = expression.find("%");
            if indexPowSymbol == -1 or indexModSymbol == -1:
                raise Exception("Неверное выражение")
            number = int(expression[0:indexPowSymbol:1]);
            degree = int(expression[indexPowSymbol+1:indexModSymbol:1]);
            module = int(expression[indexModSymbol+1:len(expression):1]);
            result = quickPow(number, degree, module);
            self.result_edit.setText(str(result));
        except Exception as ex:
            desktop = QApplication.desktop();
            widget = QWidget()
            widget.resize(desktop.width(), desktop.height())
            msg = QMessageBox.warning(widget, "Внимание!", "Неверное выражение", QMessageBox.Ok)
