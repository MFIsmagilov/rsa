# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QDialog, QMessageBox, QApplication, QWidget
from Ui_EuclidForm import Ui_EuclidForm;
from Functions import EuclidExtended


class EuclidForm(Ui_EuclidForm):

    def setupUi(self, MainWindow):
        super(EuclidForm, self).setupUi(MainWindow);
        self.initUi();

    def initUi(self):
        self.resultEuclid_button.clicked.connect(self.resultEuclid_buttonClicked);

    def resultEuclid_buttonClicked(self):
        try:
            a = int(self.a_edit.text());
            b = int(self.b_edit.text());
            
            d = EuclidExtended(a,b);
            self.result_edit.setText("d = "   +str(d));
        except:
            desktop = QApplication.desktop();
            widget = QWidget()
            widget.resize(desktop.width(), desktop.height())
            msg = QMessageBox.warning(widget, "Внимание!", "Неверные параметры", QMessageBox.Ok);