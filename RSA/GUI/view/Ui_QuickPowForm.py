# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'QuickPowForm.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_QuickPowForm(object):
    def setupUi(self, QuickPowForm):
        QuickPowForm.setObjectName("QuickPowForm")
        QuickPowForm.resize(258, 188)
        QuickPowForm.setStyleSheet("QPushButton {\n"
"    background-color:#ffec64;\n"
"    border-radius:4px;\n"
"    border:1px solid #ffaa22;\n"
"    color:#333333;\n"
"    font-family:\"Segoe UI\";\n"
"    font-size:14px;\n"
"    height:30px;\n"
"    line-height:30px;\n"
"    width:90px;\n"
"    text-decoration:none;\n"
"    text-align:center;\n"
"}\n"
"QPushButton:disabled{\n"
"    background-color:#cb881b;\n"
"    border:1px solid #bd9009;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color:#ffab23;\n"
"}\n"
"\n"
"QLineEdit{\n"
"    font-family:\"Segoe UI\";\n"
"    font-size:14px;\n"
"}\n"
"")
        self.gridLayout = QtWidgets.QGridLayout(QuickPowForm)
        self.gridLayout.setObjectName("gridLayout")
        self.result_edit = QtWidgets.QLineEdit(QuickPowForm)
        self.result_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.result_edit.setObjectName("result_edit")
        self.gridLayout.addWidget(self.result_edit, 7, 0, 1, 2)
        self.expression_edit = QtWidgets.QLineEdit(QuickPowForm)
        self.expression_edit.setAlignment(QtCore.Qt.AlignCenter)
        self.expression_edit.setObjectName("expression_edit")
        self.gridLayout.addWidget(self.expression_edit, 1, 0, 1, 2)
        self.label_3 = QtWidgets.QLabel(QuickPowForm)
        self.label_3.setMinimumSize(QtCore.QSize(0, 0))
        self.label_3.setMaximumSize(QtCore.QSize(16777215, 17))
        font = QtGui.QFont()
        font.setFamily("Segoe UI")
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 0, 0, 1, 2)
        self.resultQuickPow_button = QtWidgets.QPushButton(QuickPowForm)
        self.resultQuickPow_button.setMinimumSize(QtCore.QSize(150, 32))
        self.resultQuickPow_button.setMaximumSize(QtCore.QSize(150, 32))
        self.resultQuickPow_button.setObjectName("resultQuickPow_button")
        self.gridLayout.addWidget(self.resultQuickPow_button, 5, 1, 1, 1)
        self.label_4 = QtWidgets.QLabel(QuickPowForm)
        self.label_4.setMinimumSize(QtCore.QSize(0, 0))
        self.label_4.setMaximumSize(QtCore.QSize(16777215, 17))
        font = QtGui.QFont()
        font.setFamily("Segoe UI")
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 6, 0, 1, 1)
        self.example_label = QtWidgets.QLabel(QuickPowForm)
        self.example_label.setMinimumSize(QtCore.QSize(0, 0))
        self.example_label.setMaximumSize(QtCore.QSize(140, 15))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(11)
        font.setItalic(True)
        self.example_label.setFont(font)
        self.example_label.setObjectName("example_label")
        self.gridLayout.addWidget(self.example_label, 4, 1, 1, 1, QtCore.Qt.AlignRight)

        self.retranslateUi(QuickPowForm)
        QtCore.QMetaObject.connectSlotsByName(QuickPowForm)
        QuickPowForm.setTabOrder(self.expression_edit, self.resultQuickPow_button)
        QuickPowForm.setTabOrder(self.resultQuickPow_button, self.result_edit)

    def retranslateUi(self, QuickPowForm):
        _translate = QtCore.QCoreApplication.translate
        QuickPowForm.setWindowTitle(_translate("QuickPowForm", "Быстрое возведние в степень"))
        self.label_3.setText(_translate("QuickPowForm", "Введите выражение:"))
        self.resultQuickPow_button.setText(_translate("QuickPowForm", "Вычислить"))
        self.label_4.setText(_translate("QuickPowForm", "Результат:"))
        self.example_label.setText(_translate("QuickPowForm", "Пример: 354^34%43"))

