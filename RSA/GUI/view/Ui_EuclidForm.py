# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'EuclidForm.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_EuclidForm(object):
    def setupUi(self, EuclidForm):
        EuclidForm.setObjectName("EuclidForm")
        EuclidForm.resize(507, 152)
        EuclidForm.setMinimumSize(QtCore.QSize(507, 152))
        EuclidForm.setMaximumSize(QtCore.QSize(507, 152))
        EuclidForm.setStyleSheet("QPushButton {\n"
"    background-color:#ffec64;\n"
"    border-radius:4px;\n"
"    border:1px solid #ffaa22;\n"
"    color:#333333;\n"
"    font-family:\"Segoe UI\";\n"
"    font-size:14px;\n"
"    height:30px;\n"
"    line-height:30px;\n"
"    width:90px;\n"
"    text-decoration:none;\n"
"    text-align:center;\n"
"}\n"
"QPushButton:disabled{\n"
"    background-color:#cb881b;\n"
"    border:1px solid #bd9009;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"    background-color:#ffab23;\n"
"}\n"
"\n"
"QLineEdit{\n"
"    font-family:\"Segoe UI\";\n"
"    font-size:14px;\n"
"}\n"
"")
        self.a_edit = QtWidgets.QLineEdit(EuclidForm)
        self.a_edit.setGeometry(QtCore.QRect(38, 10, 191, 20))
        self.a_edit.setObjectName("a_edit")
        self.b_edit = QtWidgets.QLineEdit(EuclidForm)
        self.b_edit.setGeometry(QtCore.QRect(308, 10, 191, 20))
        self.b_edit.setObjectName("b_edit")
        self.label = QtWidgets.QLabel(EuclidForm)
        self.label.setGeometry(QtCore.QRect(16, 10, 21, 16))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(EuclidForm)
        self.label_2.setGeometry(QtCore.QRect(288, 10, 20, 20))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(EuclidForm)
        self.label_3.setGeometry(QtCore.QRect(220, 30, 91, 31))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.resultEuclid_button = QtWidgets.QPushButton(EuclidForm)
        self.resultEuclid_button.setGeometry(QtCore.QRect(363, 100, 131, 41))
        self.resultEuclid_button.setObjectName("resultEuclid_button")
        self.result_edit = QtWidgets.QLineEdit(EuclidForm)
        self.result_edit.setGeometry(QtCore.QRect(38, 70, 461, 20))
        self.result_edit.setObjectName("result_edit")

        self.retranslateUi(EuclidForm)
        QtCore.QMetaObject.connectSlotsByName(EuclidForm)

    def retranslateUi(self, EuclidForm):
        _translate = QtCore.QCoreApplication.translate
        EuclidForm.setWindowTitle(_translate("EuclidForm", "Расширенный алгоритм Евклида"))
        self.label.setText(_translate("EuclidForm", "A"))
        self.label_2.setText(_translate("EuclidForm", "B"))
        self.label_3.setText(_translate("EuclidForm", "Ax + By = D"))
        self.resultEuclid_button.setText(_translate("EuclidForm", "Вычислить"))

